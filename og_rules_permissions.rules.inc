<?php

/**
 * Implements hook_rules_action_info().
 */
function og_rules_permissions_rules_action_info() {
  $items = array();
  $items['og_rules_permissions_og_set_permission'] = array(
    'label' => t('Set OG Permission'),
    'group' => t('Organic groups'),
    'parameter' => array(
      'group_content' => array(
        'type' => 'entity',
        'label' => t('Group content'),
        'description' => t('The group content to set permission on.'),
      ),
      'role' => array(
        'type' => 'text',
        'label' => t('Group role'),
        'options list' => 'og_rules_permissions_get_roles',
        'restriction' => 'input',
      ),
      'permission' => array(
        'type' => 'text',
        'label' => t('Permission to set'),
        'options list' => 'og_rules_permissions_get_permissions',
        'restriction' => 'input',
      ),
      'enable' => array(
        'type' => 'boolean',
        'label' => t('Enable this permission'),
        'restriction' => 'input',
      ),
    ),
  );

  return $items;
}

/**
 * Sets OG permission.
 */
function og_rules_permissions_og_set_permission($group, $role, $permission, $enable) {
  $permission_change = array($permission => $enable);
  $gid = $group->getIdentifier();

  // Grab role id for group and permission.
  $query = db_select('og_role', 'ogr')
    ->fields('ogr')
    ->condition('gid', $gid, '=')
    ->condition('name', $role, '=')
    ->execute();

  $result = $query->fetchAssoc();

  if ($result) {
    // Change the permission.
    og_role_change_permissions($result['rid'], $permission_change);
  }

}
